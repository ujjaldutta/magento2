<?php
/**
 * Copyright © 2015 Ud. All rights reserved.
 */
namespace Ud\Faq\Model\ResourceModel;

/**
 * Faq resource
 */
class Faq extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * Initialize resource
     *
     * @return void
     */
    public function _construct()
    {
        $this->_init('faq_faq', 'id');
    }

  
}
