<?php
/**
 * Copyright © 2016 RealWebStack. All rights reserved.
 * LICENSE: distributions of the source code without 
 * authors permission is forbidden, For written 
 * permission please contact support@realwebstack.com.
 */
\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'Ud_PaypalAdaptive',
    __DIR__
);
