<?php
/**
 * Copyright © 2016 RealWebStack. All rights reserved.
 * LICENSE: distributions of the source code without 
 * authors permission is forbidden, For written 
 * permission please contact support@realwebstack.com.
 */

namespace Ud\PaypalAdaptive\Model;

use PayPal\Service\AdaptivePaymentsService;
use PayPal\Types\AP\FundingConstraint;
use PayPal\Types\AP\FundingTypeInfo;
use PayPal\Types\AP\FundingTypeList;
use PayPal\Types\AP\PayRequest;
use PayPal\Types\AP\Receiver;
use PayPal\Types\AP\ReceiverList;
use PayPal\Types\AP\SenderIdentifier;
use PayPal\Types\Common\PhoneNumberType;
use PayPal\Types\Common\RequestEnvelope;
use PayPal\Types\AP\PaymentDetailsRequest;
use PayPal\Types\AP\ExecutePaymentRequest;
use PayPal\Types\AP\RefundRequest;
use PayPal\Types\AP\GetFundingPlansRequest;

/**
 * Pay In Store payment method model
 */
class AdaptiveCapture extends \Magento\Payment\Model\Method\AbstractMethod {

    const METHOD_CODE = 'adaptivecapture';
    const PAYPAL_REDIRECT_SANDURL = 'https://www.sandbox.paypal.com/webscr&cmd=';
    const DEVELOPER_PORTAL = 'https://developer.paypal.com';
    const PAYPAL_REDIRECT_URL = 'https://www.paypal.com/webscr&cmd=';

    /**
     * @var string
     */
    protected $_code = self::METHOD_CODE;
    protected $_redirectUrl;

    /**
     * Availability option
     *
     * @var bool
     */
    //protected $_isOffline = true;


    protected $_isGateway = true;
    protected $_canCapture = true;
    protected $_canCapturePartial = true;
    protected $_canRefund = true;
    protected $_canRefundInvoicePartial = true;
    protected $_canAuthorize = true;
   
    protected $_minAmount = null;
    protected $_maxAmount = null;
    protected $_supportedCurrencyCodes = array('USD');
    protected $_debugReplacePrivateDataKeys = ['number', 'exp_month', 'exp_year', 'cvc'];

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * @var \Magento\Framework\UrlInterface
     */
    protected $_urlBuilder;

    /**
     * @var \Magento\Paypal\Model\CartFactory
     */
    protected $_cartFactory;

    /**
     * @var \Magento\Checkout\Model\Session
     */
    protected $_checkoutSession;

    /**
     * @var \Magento\Framework\Exception\LocalizedExceptionFactory
     */
    protected $_exception;

    /**
     * @var \Magento\Sales\Api\TransactionRepositoryInterface
     */
    protected $transactionRepository;

    /**
     * @var Transaction\BuilderInterface
     */
    protected $transactionBuilder;

    public function __construct(
            \Magento\Framework\Model\Context $context, 
            \Magento\Framework\Registry $registry,
            \Magento\Framework\Api\ExtensionAttributesFactory $extensionFactory,
            \Magento\Framework\Api\AttributeValueFactory $customAttributeFactory,
            \Magento\Payment\Helper\Data $paymentData,
            \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
            \Magento\Payment\Model\Method\Logger $logger,
            \Magento\Store\Model\StoreManagerInterface $storeManager,
            \Magento\Framework\UrlInterface $urlBuilder,
            \Magento\Paypal\Model\CartFactory $cartFactory,
            \Magento\Checkout\Model\Session $checkoutSession,
            \Magento\Framework\Exception\LocalizedExceptionFactory $exception,
            \Magento\Sales\Api\TransactionRepositoryInterface $transactionRepository,
            \Magento\Sales\Model\Order\Payment\Transaction\BuilderInterface $transactionBuilder,
            \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
            \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
            array $data = []
    ) {
        parent::__construct(
                $context, $registry, $extensionFactory, $customAttributeFactory, $paymentData, $scopeConfig, $logger, $resource, $resourceCollection, $data
        );
        $this->_storeManager = $storeManager;
        $this->_urlBuilder = $urlBuilder;
        $this->_cartFactory = $cartFactory;
        $this->_checkoutSession = $checkoutSession;
        $this->_exception = $exception;
        $this->transactionRepository = $transactionRepository;
        $this->transactionBuilder = $transactionBuilder;

        /* $this->_minAmount = $this->getConfigData('min_order_total');
          $this->_maxAmount = $this->getConfigData('max_order_total'); */
    }

    private function getConfig() {

        $config = array("mode" => $this->getConfigData('paypalmode'));


        return $config;
    }

    // Creates a configuration array containing credentials and other required configuration parameters.
    private function getAcctAndConfig() {
        $config = array(
            // Signature Credential
            "acct1.UserName" => $this->getConfigData('apiusername'),
            "acct1.Password" => $this->getConfigData('apipassword'),
            "acct1.Signature" => $this->getConfigData('apisignature'),
            "acct1.AppId" => $this->getConfigData('appid')
        );

        return array_merge($config, $this->getConfig());
        ;
    }

    /**
     * Payment capturing
     *
     * @param \Magento\Payment\Model\InfoInterface $payment
     * @param float $amount
     * @return $this
     * @throws \Magento\Framework\Validator\Exception
     */
    public function capture(\Magento\Payment\Model\InfoInterface $payment, $amount) {
       
        /** @var \Magento\Sales\Model\Order $order */
        $order = $payment->getOrder();
        $key = $payment->getLastTransId();
        /** @var \Magento\Sales\Model\Order\Address $billing */
        $billing = $order->getBillingAddress();
        
       $paytype=$this->getConfigData('paypaltype');
        if($paytype=='parallel') return $this;

        try {


            $executePaymentRequest = new ExecutePaymentRequest(new RequestEnvelope("en_US"), $key);
            $executePaymentRequest->actionType = 'PAY';

            $service = new AdaptivePaymentsService($this->getAcctAndConfig());

            try {
                /* wrap API method calls on the service object with a try catch */

                $response = $service->ExecutePayment($executePaymentRequest);
            } catch (Exception $ex) {
                throw new \Magento\Framework\Validator\Exception(__('Payment capturing error.'));
            }

            $ack = strtoupper($response->responseEnvelope->ack);

            if ($ack != "SUCCESS") {
                throw new \Magento\Framework\Validator\Exception(__('Payment capturing error.'));
            } else {


                $tid = $response->responseEnvelope->correlationId;
                $payment
                        ->setTransactionId($key . "," . $tid)
                        ->setIsTransactionClosed(1)
                        ->setShouldCloseParentTransaction(1);
            }
        } catch (\Exception $e) {
            $this->debugData(['exception' => $e->getMessage()]);
            $this->_logger->error(__('Payment capturing error.'));
            throw new \Magento\Framework\Validator\Exception(__('Payment capturing error.'));
        }

        return $this;
    }

    /**
     * Payment refund
     *
     * @param \Magento\Payment\Model\InfoInterface $payment
     * @param float $amount
     * @return $this
     * @throws \Magento\Framework\Validator\Exception
     */
    public function refund(\Magento\Payment\Model\InfoInterface $payment, $amount) {
        
        $paytype=$this->getConfigData('paypaltype');
        
        
        $transactionId = $payment->getParentTransactionId();
        $tid = explode(",", $transactionId);
        $payKey = $tid[0];

        $refundRequest = new RefundRequest(new RequestEnvelope("en_US"));
        $refundRequest->currencyCode = $this->_storeManager->getStore()->getCurrentCurrency()->getCode();
        $refundRequest->payKey = $payKey;

        $order = $payment->getOrder();
        $orderItems = $order->getAllItems();
        $i = 0;
        $receiver = array();
        
        if($paytype=='chain'){
        $receiver[$i] = new Receiver();
        $receiver[$i]->email = $this->getConfigData('receive_email');
        $receiver[$i]->amount = $order->getGrandTotal();
        $receiver[$i]->primary = true;
        $receiver[$i]->invoiceId = $order->getId();
        }else{
            $receiver[$i] = new Receiver();
            $receiver[$i]->email = $this->getConfigData('receive_email');
            $receiver[$i]->amount = $order->getGrandTotal();
            $receiver[$i]->primary = false;
            $receiver[$i]->invoiceId = $order->getId();

            $storecomission = (float) $this->getConfigData('receive_percent');
            $vendorcomission = 100 - $storecomission;
            foreach ($orderItems as $item) {
                $productid = $item->getProductId();
                $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
                $product = $objectManager->get('Magento\Catalog\Model\Product')->load($productid);
                $i++;



                $receiver[$i] = new Receiver();
                $receiver[$i]->email = $product->getSellerEmail();
                $receiver[$i]->amount = $item->getBaseRowTotalInclTax() * $vendorcomission / 100;
                $receiver[$i]->primary = false;
                $receiver[$i]->invoiceId = $order->getId();
            }
            
        }
        
        
        
        

        $receiverList = new ReceiverList($receiver);

        try {
            $service = new AdaptivePaymentsService($this->getAcctAndConfig());
            $response = $service->Refund($refundRequest);
        } catch (\Exception $e) {
            $this->debugData(['transaction_id' => $transactionId, 'exception' => $e->getMessage()]);
            $this->_logger->error(__('Payment refunding error.'));
            throw new \Magento\Framework\Validator\Exception(__('Payment refunding error.'));
        }
        $ack = strtoupper($response->responseEnvelope->ack);

        if ($ack == 'SUCCESS') {
            $tid = $response->responseEnvelope->correlationId;

            $payment
                    ->setTransactionId($payKey . "," . $tid . '-' . \Magento\Sales\Model\Order\Payment\Transaction::TYPE_REFUND)
                    ->setParentTransactionId($payKey . "," . $tid)
                    ->setIsTransactionClosed(1)
                    ->setShouldCloseParentTransaction(1);

            return $this;
        }

        throw new \Magento\Framework\Validator\Exception(__('Payment refunding error.'));
    }

    /**
     * Determine method availability based on quote amount and config data
     *
     * @param \Magento\Quote\Api\Data\CartInterface|null $quote
     * @return bool
     */
    public function isAvailable(\Magento\Quote\Api\Data\CartInterface $quote = null) {
        return parent::isAvailable($quote);

        /* if ($quote && (
          $quote->getBaseGrandTotal() < $this->_minAmount
          || ($this->_maxAmount && $quote->getBaseGrandTotal() > $this->_maxAmount))
          ) {
          return false;
          } */

        if ($this->getConfigData('appid')=='' || $this->getConfigData('receive_email')=='' || $this->getConfigData('apisignature')=='') {
            return false;
        }

        return parent::isAvailable($quote);
    }

    /**
     * Availability for currency
     *
     * @param string $currencyCode
     * @return bool
     */
    public function canUseForCurrency($currencyCode) {

        if (!in_array($currencyCode, $this->_supportedCurrencyCodes)) {
            return false;
        }
        return true;
    }

    public function authorize(\Magento\Payment\Model\InfoInterface $payment, $amount) {

        //return $this->_placeOrder($payment, $amount);
    }

    public function getpaypalRedirectUrl($order = null) {

        $paytype=$this->getConfigData('paypaltype');
        $orderItems = $order->getAllItems();
        $i = 0;
        $storecomission = (float) $this->getConfigData('receive_percent');
        $vendorcomission = 100 - $storecomission;
        $amount=$order->getGrandTotal();
        $shipping=$order->getShippingAmount();
        $orderamt=$amount- $shipping;
        if($paytype=='chain'){            
            $amount= $order->getGrandTotal();
        }else{
            
            $amount=$orderamt* $storecomission / 100;
             $amount= $amount+$shipping;
        }
      
        
        $receiver = array();
        $receiver[$i] = new Receiver();
        $receiver[$i]->email = $this->getConfigData('receive_email');
        $receiver[$i]->amount = $amount;
        $receiver[$i]->primary = ($paytype=='chain'?true:false);
        $receiver[$i]->invoiceId = $order->getId();

       
        foreach ($orderItems as $item) {
            $productid = $item->getProductId();
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $product = $objectManager->get('Magento\Catalog\Model\Product')->load($productid);
            $i++;



            $receiver[$i] = new Receiver();
            $receiver[$i]->email = $product->getSellerEmail();
            $receiver[$i]->amount = $item->getBaseRowTotalInclTax() * $vendorcomission / 100;
            $receiver[$i]->primary = false;
            $receiver[$i]->invoiceId = $order->getId();
        }
      
        if (count($receiver) < 2) {
            throw new \Magento\Framework\Validator\Exception(__('Invalid Receiver.'));
            return false;
        }
        $receiverList = new ReceiverList($receiver);
        $actionType=($paytype=='chain'?'PAY_PRIMARY':'PAY');
        $payRequest = new PayRequest(new RequestEnvelope("en_US"), $actionType, $this->_urlBuilder->getUrl("checkout/onepage/failure"), 'USD', $receiverList, $this->_urlBuilder->getUrl("paypaladaptive/index/"));
        $customerid = $order->getCustomerId();
        //customer name
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $customer = $objectManager->create('Magento\Customer\Model\Customer')->load($customerid);


        $payRequest->senderEmail = $customer->getEmail();
        $payRequest->feesPayer = 'EACHRECEIVER';
        $payRequest->reverseAllParallelPaymentsOnError = false;

        $service = new AdaptivePaymentsService($this->getAcctAndConfig());

        try {
          
            $response = $service->Pay($payRequest);
        } catch (\Exception $e) {
            throw new \Magento\Framework\Validator\Exception(__('Error in gateway.'));
        }

        $ack = strtoupper($response->responseEnvelope->ack);

        if ($ack != "SUCCESS") {
            return false;
        } else {
            $payKey = $response->payKey;
            $token = $response->payKey;
            $this->_checkoutSession->setLastOrderId($order->getId());
            $this->_checkoutSession->setPayKey($payKey);
            $mode = $this->getConfigData('paypalmode');
            if ($mode == 'sandbox') {
                $payPalURL = self::PAYPAL_REDIRECT_SANDURL . '_ap-payment&paykey=' . $token;
            } else {
                $payPalURL = self::PAYPAL_REDIRECT_URL . '_ap-payment&paykey=' . $token;
            }

            return $payPalURL;
        }
    }

    public function fetchResponse($order, $paykey) {
        $requestEnvelope = new RequestEnvelope("en_US");
        $paymentDetailsReq = new PaymentDetailsRequest($requestEnvelope);

        $paymentDetailsReq->payKey = $paykey;
        $service = new AdaptivePaymentsService($this->getAcctAndConfig());

        try {
      
            $response = $service->PaymentDetails($paymentDetailsReq);
        } catch (\Exception $e) {

            throw new \Magento\Framework\Validator\Exception(__('Error in gateway.'));
        }
        $ack = strtoupper($response->responseEnvelope->ack);

        if ($ack != "SUCCESS") {
            return false;
        } else {




            $payment = $order->getPayment();
            $payment->setLastTransId($paykey);
            $payment->setTransactionId($paykey);
            $fpaydata=array();
            $iloop=1;
            foreach($response->paymentInfoList->paymentInfo as $info){
               
               $receiverdata=(array)$info->receiver;
                              
               $pData=array("TranscactionId"=>$info->transactionId,"Status"=>$info->transactionStatus); 
               $paymentData=  array_merge($pData,$receiverdata);
                foreach($paymentData as $key=>$value){
                    $fpaydata[$key.$iloop]=$value;

                }
               $iloop++;
            
            }
             $paymentData=$fpaydata;
            $payment->setAdditionalInformation([\Magento\Sales\Model\Order\Payment\Transaction::RAW_DETAILS => (array) $paymentData]);
            //$payment->setAdditionalInformation(array("response" => serialize($response->paymentInfoList)));
            
            $formatedPrice = $order->getBaseCurrency()->formatTxt(
                    $order->getGrandTotal()
            );
            $message = __('The authorized amount is %1.', $formatedPrice);
            
            $trans = $this->transactionBuilder;
            $transaction = $trans->setPayment($payment)
                    ->setOrder($order)
                    ->setTransactionId($paykey)
                    ->setAdditionalInformation([\Magento\Sales\Model\Order\Payment\Transaction::RAW_DETAILS => (array) $paymentData])
                    ->setFailSafe(true)
                    
                    ->build(\Magento\Sales\Model\Order\Payment\Transaction::TYPE_CAPTURE);

            $payment->addTransactionCommentsToOrder(
                    $transaction, $message
            );
            $payment->setParentTransactionId(null);
            $payment->save();
            $order->save();




            return $response->status;
        }
    }

}
