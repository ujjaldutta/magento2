<?php
/**
 * Copyright © 2016 RealWebStack. All rights reserved.
 * LICENSE: distributions of the source code without 
 * authors permission is forbidden, For written 
 * permission please contact support@realwebstack.com.
 */

namespace Ud\PaypalAdaptive\Model\Config\Source;


/**
 * Order Status source model
 */
class Paypaltype implements \Magento\Framework\Option\ArrayInterface
{
    public function toOptionArray()
    {
        return [
            ['value' => 'chain', 'label' => __('Chain')],
            ['value' => 'parallel', 'label' => __('Parallel')]
            
        ];
    }
}
