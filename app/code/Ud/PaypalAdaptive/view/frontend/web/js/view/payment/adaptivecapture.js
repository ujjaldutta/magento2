/**
 * Copyright © 2016 RealWebStack. All rights reserved.
 * LICENSE: distributions of the source code without 
 * authors permission is forbidden, For written 
 * permission please contact support@realwebstack.com.
 */
/*browser:true*/
/*global define*/
define(
    [
        'uiComponent',
        'Magento_Checkout/js/model/payment/renderer-list'
    ],
    function (
        Component,
        rendererList
    ) {
        'use strict';
        rendererList.push(
            {
                type: 'adaptivecapture',
                component: 'Ud_PaypalAdaptive/js/view/payment/method-renderer/adaptivecapture-method'
            }
        );
        /** Add view logic here if needed */
        return Component.extend({});
    }
);


