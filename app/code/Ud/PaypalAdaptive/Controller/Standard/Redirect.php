<?php
/**
 * Copyright © 2016 RealWebStack. All rights reserved.
 * LICENSE: distributions of the source code without 
 * authors permission is forbidden, For written 
 * permission please contact support@realwebstack.com.
 */

namespace Ud\PaypalAdaptive\Controller\Standard;

class Redirect extends \Magento\Framework\App\Action\Action {

    /**
     * @var \Magento\Framework\App\Cache\TypeListInterface
     */
    protected $_cacheTypeList;

    /**
     * @var \Magento\Framework\App\Cache\StateInterface
     */
    protected $_cacheState;

    /**
     * @var \Magento\Framework\App\Cache\Frontend\Pool
     */
    protected $_cacheFrontendPool;

    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $resultPageFactory;
    protected $_checkout;
    protected $_checkoutType;
    protected $_quote = false;
    protected $_checkoutSession;

    /**
     * @param Action\Context $context
     * @param \Magento\Framework\App\Cache\TypeListInterface $cacheTypeList
     * @param \Magento\Framework\App\Cache\StateInterface $cacheState
     * @param \Magento\Framework\App\Cache\Frontend\Pool $cacheFrontendPool
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    public function __construct(
    \Magento\Framework\App\Action\Context $context,
            \Magento\Checkout\Model\Session $checkoutSession,
            \Magento\Sales\Model\OrderFactory $orderFactory,
            \Magento\Paypal\Model\Express\Checkout\Factory $checkoutFactory,
            \Magento\Framework\Session\Generic $paypalSession,
            \Magento\Framework\Url\Helper\Data $urlHelper,
            \Magento\Customer\Model\Url $customerUrl,
            \Magento\Framework\App\Cache\TypeListInterface $cacheTypeList,
            \Magento\Framework\App\Cache\StateInterface $cacheState,
            \Magento\Framework\App\Cache\Frontend\Pool $cacheFrontendPool,
            \Magento\Framework\View\Result\PageFactory $resultPageFactory,
            \Ud\PaypalAdaptive\Model\AdaptiveCapture $adaptive
    ) {
        parent::__construct($context
                //, $checkoutSession, $orderFactory, $checkoutFactory, $paypalSession, $urlHelper, $customerUrl
        );
        $this->_cacheTypeList = $cacheTypeList;
        $this->_cacheState = $cacheState;
        $this->_cacheFrontendPool = $cacheFrontendPool;
        $this->resultPageFactory = $resultPageFactory;


        $this->_checkoutSession = $checkoutSession;
        $this->_orderFactory = $orderFactory;
        $this->_checkoutFactory = $checkoutFactory;
        $this->_paypalSession = $paypalSession;
        $this->_urlHelper = $urlHelper;
        $this->_customerUrl = $customerUrl;
        $this->adaptive = $adaptive;
    }

    /**
     * Redirect
     *
     */
    public function execute() {
        try {

            $order = $this->_checkoutSession->getLastRealOrder();
         

            $url = $this->adaptive->getpaypalRedirectUrl($order);

            if ($url) {
                $this->getResponse()->setRedirect($url);
                return;
            } else {
         
                $this->_redirect('checkout/onepage/failure');

                return;
            }
        } catch (\Exception $e) {
           
             throw new \Magento\Framework\Validator\Exception(__('We can\'t complete the order.'));
            //$this->_redirect('checkout');
        }
    }

}
