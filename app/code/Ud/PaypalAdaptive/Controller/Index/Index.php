<?php
/**
 * Copyright © 2016 RealWebStack. All rights reserved.
 * LICENSE: distributions of the source code without 
 * authors permission is forbidden, For written 
 * permission please contact support@realwebstack.com.
 */

namespace Ud\PaypalAdaptive\Controller\Index;

class Index extends \Magento\Framework\App\Action\Action {

    /**
     * @var \Magento\Framework\App\Cache\TypeListInterface
     */
    protected $_cacheTypeList;

    /**
     * @var \Magento\Framework\App\Cache\StateInterface
     */
    protected $_cacheState;

    /**
     * @var \Magento\Framework\App\Cache\Frontend\Pool
     */
    protected $_cacheFrontendPool;

    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $resultPageFactory;
    protected $_checkoutSession;
    protected $_urlBuilder;
    protected $scopeConfig;

    /**
     * @param Action\Context $context
     * @param \Magento\Framework\App\Cache\TypeListInterface $cacheTypeList
     * @param \Magento\Framework\App\Cache\StateInterface $cacheState
     * @param \Magento\Framework\App\Cache\Frontend\Pool $cacheFrontendPool
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    public function __construct(
    \Magento\Framework\App\Action\Context $context, \Magento\Framework\App\Cache\TypeListInterface $cacheTypeList, \Magento\Framework\App\Cache\StateInterface $cacheState, \Magento\Framework\App\Cache\Frontend\Pool $cacheFrontendPool, \Magento\Framework\View\Result\PageFactory $resultPageFactory, \Ud\PaypalAdaptive\Model\AdaptiveCapture $adaptive, \Magento\Checkout\Model\Session $checkoutSession,\Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig, \Magento\Store\Model\StoreManagerInterface $storeManager
    ) {
        parent::__construct($context);
        $this->_cacheTypeList = $cacheTypeList;
        $this->_cacheState = $cacheState;
        $this->_cacheFrontendPool = $cacheFrontendPool;
        $this->resultPageFactory = $resultPageFactory;
        $this->adaptive = $adaptive;
        $this->_storeManager = $storeManager;
        $this->scopeConfig =$scopeConfig;
        $this->_checkoutSession = $checkoutSession;
    }

    /**
     * Run on success return
     *
     */
    public function execute() {
        $order_id = $this->_checkoutSession->getLastOrderId();
        $paykey = $this->_checkoutSession->getPayKey();

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $order = $objectManager->get('Magento\Sales\Model\Order');
        $order = $order->load($order_id);

        $response = $this->adaptive->fetchResponse($order, $paykey);

        //$paytype = $this->getConfigData('paypaltype');
         $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;
         $paytype =$this->scopeConfig->getValue('payment/adaptivecapture/paypaltype', $storeScope);
        
        if ($paytype == 'chain' && $response == 'INCOMPLETE') {
            $this->_redirect('checkout/onepage/success');
            return;
        } elseif ($paytype == 'parallel' && $response == 'COMPLETED') {

            $this->_redirect('checkout/onepage/success');
            return;
        }else{
            
               $this->_redirect('checkout/onepage/failure');
        }
    }

}
