<?php
namespace Custom\Page\Block;
 
class Page extends \Magento\Framework\View\Element\Template
{
	protected $_categoryHelper;
     protected $categoryFlatConfig;
     protected $topMenu;

     /**
     * Category factory
     *
     * @var \Magento\Catalog\Model\CategoryFactory
     */
    protected $_categoryFactory;

 

    /**
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Magento\Catalog\Helper\Category $categoryHelper
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Catalog\Helper\Category $categoryHelper,
        \Magento\Catalog\Model\Indexer\Category\Flat\State $categoryFlatState,
        \Magento\Theme\Block\Html\Topmenu $topMenu,
        \Magento\Catalog\Model\CategoryFactory $categoryFactory
    ) {

        $this->_categoryHelper = $categoryHelper;
        $this->_categoryFactory = $categoryFactory;
        $this->categoryFlatConfig = $categoryFlatState;
        $this->topMenu = $topMenu;
       
        parent::__construct($context);
    }
    /**
     * Return categories helper
     */   
    public function getCategoryHelper()
    {
        return $this->_categoryHelper;
    }
    
    
	public function getCategory()
	{
		$categoryId = 3;
		$category = $this->_categoryFactory->create()->load($categoryId);
		return $category;
	}
	public function getProductsCollection()
	{
		
		return $this->getCategory()->getProductCollection()->addAttributeToSelect('*');
	}
	
	
	public function _prepareLayout()
	{
		return parent::_prepareLayout();
	}
	
	public function getContent(){

		return "Welcome to block class function";
	}
	
	public function productlist(){
		
		
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		/** @var \Magento\Catalog\Model\ResourceModel\Product\Collection $productCollection */
		$productCollection = $objectManager->create('Magento\Catalog\Model\ResourceModel\Product\Collection');
		/** Apply filters here */
		return $productCollection->load();
	}
}
 
